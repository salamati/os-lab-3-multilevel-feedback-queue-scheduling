#include "types.h"
#include "stat.h"
#include "user.h"
#include "fcntl.h"
#include "fs.h"

char buf[512];

int main(int argc, char *argv[])
{
  if (argc < 2)
  {
    printf(1, "Usage: touch [files]...\n");
    exit();
  }
  else if (argc == 2)
  {
    int file = open(argv[1], O_RDWR);
    if (file < 0)
    {
      file = open(argv[1], O_CREATE);
    }
    else
    {
      printf(1, "This file already exists...\n");
    }
    exit();
  }
  else if (argc == 3 && argv[1][0] == '-' && argv[1][1] == 'w')
  {
    int file = open(argv[2], O_RDWR);
    if (file > 0)
    {
      unlink(argv[2]);
    }
    file = open(argv[2], O_CREATE | O_RDWR);
    int input = read(0, buf, sizeof(buf));
    if (input < 0)
    {
      printf(1, "Read error\n");
    }
    else if (write(file, buf, input) != input)
    {
      printf(1, "Write error\n");
    }
    exit();
  }
}