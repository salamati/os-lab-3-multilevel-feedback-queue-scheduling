#include "types.h"
#include "stat.h"
#include "user.h"

int main(int argc, char *argv[])
{
    int pid;
    for (int n = 0; n < 6; n++)
    {
        pid = fork();
        if (pid < 0)
            break;
        if (pid == 0)
            exit();
    }
    for (int n = 0; n < 6; n++)
    {
        if (wait() < 0)
        {
            printf(1, "wait stopped early\n");
            exit();
        }
    }
    printf(1, "hello\n");
    printsyscalls();
    exit();
}
