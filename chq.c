#include "types.h"
#include "stat.h"
#include "user.h"
#include "fs.h"
#include "fcntl.h"

int main(int argc, char *argv[])
{
    if (argc != 3)
    {
        printf(1, "Wrong input!\n", sizeof("Wrong input!\n"));
        exit();
    }

    int pid, queue;
    pid = atoi(argv[1]);
    queue = atoi(argv[2]);
    changequeue(pid, queue);
    printf(1, "Done! \n");
    exit();
}