#include "types.h"
#include "stat.h"
#include "user.h"
#include "fs.h"
#include "fcntl.h"
#include "date.h"

void fakeProc(int q)
{
    if (fork() == 0)
    {
        int i;
        int pid = getpid();
        changequeue(pid, q);
        for (i = 0; i < 800000; i++)
        {
            set_edx(10);
        }
        printf(1, "DONE!! pid:%d Number is: %d \n", pid, i);
        exit();
    }
}

int main(int argc, char *argv[])
{
    fakeProc(0);
    fakeProc(0);
    fakeProc(2);
    exit();
}