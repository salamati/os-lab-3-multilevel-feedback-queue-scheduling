#include "types.h"
#include "stat.h"
#include "user.h"

void periodic();

int main(int argc, char *argv[])
{
  int i;
  printf(1, "alarmtest starting\n");
  alarm(20, periodic);
  for (i = 0; i < 1e10; i++)
  {
    if ((i++ % 5000000) == 0)
      write(2, ".", 1);
  }
  exit();
}

void periodic()
{
  printf(1, "alarm!\n");
}

